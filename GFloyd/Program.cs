﻿using System;
using System.IO;
using System.Text;
using GFloydLib.Lexer;
using GFloydLib.Parser;
using GFloydLib.Compiler;
using GFloydLib.Compiler.Compilers;
using GFloydLib.GMachine;


namespace GFloyd
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0) {
                Console.WriteLine("No path specified");
                return;
            }
            var path = args[0];
            var input = File.ReadAllText(path, Encoding.UTF8);
            var tokens = Lexer.Tokenize(input);
            foreach (var dt in tokens)
                Console.WriteLine(dt.Token + ":" + dt.Value);
            var parser = new Parser(tokens);
            try
            {
                var syntaxTree = parser.Parse();
                Console.WriteLine(syntaxTree.Print());
                var compiler = new LazyCompiler();
                syntaxTree.Accept(compiler, true);
                var compiledProgram = compiler.map;
                foreach (var sc in compiledProgram.Values)
                {
                    Console.WriteLine();
                    Console.Write("[");
                    foreach (var i in sc)
                        Console.Write(i + " ");
                    Console.WriteLine("]");
                }
                var virtualMachine = new GMachine(compiledProgram);
                virtualMachine.Execute();
                Console.WriteLine(virtualMachine.StackTrace());
                Console.WriteLine(virtualMachine.GetStats());
            }
            catch (ParserException pe)
            {
                Console.WriteLine("Parse error: " + pe.Message);
            }
            catch (CompilerException ce) 
            {
                Console.WriteLine("Compiler error: " + ce.Message);
            }
            catch (GMachineException ge)
            {
                Console.WriteLine("Virtual machine error: " + ge.Message);
            }
            
            Console.ReadKey();
        }
    }
}
