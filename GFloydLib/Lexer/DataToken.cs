﻿namespace GFloydLib.Lexer
{
    public struct DataToken
    {
        public string Value { get; set; }
        public Token Token { get; set; }
        public int Line { get; set; }
        public int Position { get; set; }
    }
}
