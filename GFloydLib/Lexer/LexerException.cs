﻿using System;

namespace GFloydLib.Lexer
{
    class LexerException : GFloydException
    {
        public LexerException()
        { }
        public LexerException(string message) : base(message) { }
        public LexerException(string message, Exception inner) : base(message, inner) { }
    }
}
