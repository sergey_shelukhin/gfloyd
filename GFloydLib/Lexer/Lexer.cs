﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace GFloydLib.Lexer
{
    public class Lexer
    {
        private static List<Tuple<Token, Regex>> rules;

        static Lexer(){
            rules = new List<Tuple<Token, Regex>> {
                new Tuple<Token, Regex>(Token.In, new Regex(@"in")),
                new Tuple<Token, Regex>(Token.If, new Regex(@"if")),
                new Tuple<Token, Regex>(Token.Letrec, new Regex(@"letrec")),
                new Tuple<Token, Regex>(Token.Let, new Regex(@"let")),
	            new Tuple<Token, Regex>(Token.Num, new Regex(@"[0-9]+")),
	            new Tuple<Token, Regex>(Token.Var, new Regex(@"[a-zA-Z]\w*")),
	            new Tuple<Token, Regex>(Token.LeftParent, new Regex(@"\(")),
	            new Tuple<Token, Regex>(Token.RightParent, new Regex(@"\)")),
                new Tuple<Token, Regex>(Token.Comma, new Regex(@",")),
	            new Tuple<Token, Regex>(Token.Equal, new Regex(@"==")),
	            new Tuple<Token, Regex>(Token.NotEqual, new Regex(@"!=")),
	            new Tuple<Token, Regex>(Token.GreaterOrEqual, new Regex(@">=")),
	            new Tuple<Token, Regex>(Token.LessOrEqual, new Regex(@"<=")),
	            new Tuple<Token, Regex>(Token.Greater, new Regex(@">")),
	            new Tuple<Token, Regex>(Token.Less, new Regex(@"<")),
	            new Tuple<Token, Regex>(Token.And, new Regex(@"&")),
                new Tuple<Token, Regex>(Token.Or, new Regex(@"\|")),
	            new Tuple<Token, Regex>(Token.Plus, new Regex(@"\+")),
                new Tuple<Token, Regex>(Token.Minus, new Regex(@"-")),
                new Tuple<Token, Regex>(Token.Mult, new Regex(@"\*")),
                new Tuple<Token, Regex>(Token.Derive, new Regex(@"\/")),
                new Tuple<Token, Regex>(Token.Lambda, new Regex(@"\\")),
                new Tuple<Token, Regex>(Token.Point, new Regex(@"\.")),
                new Tuple<Token, Regex>(Token.Semicolon, new Regex(@";")),
                new Tuple<Token, Regex>(Token.Assign, new Regex(@"="))
            };
        }

        public static List<DataToken> Tokenize(string input){
            var result = new List<DataToken>();
            int line = 1;
            int position = 1;
            int total = 0;
            while(total < input.Length){
                while (total < input.Length &&
                    (input[total] == ' ' || input[total] == '\t' || input[total] == '\r' || input[total] == '\n'))
                {
                    if (input[total] == '\n')
                        position = 1;
                    ++total;
                    ++position;
                }
                if (total == input.Length)
                    break;
                Match matchedToken = null;
                foreach (var rule in rules)
                {
                    if (rule.Item2.IsMatch(input, total))
                    {
                        matchedToken = rule.Item2.Match(input, total);
                        if (matchedToken != null && matchedToken.Index == total)
                        {
                            result.Add(new DataToken()
                            {
                                Value = matchedToken.Value,
                                Token = rule.Item1,
                                Position = position,
                                Line = line
                            });
                            total += matchedToken.Length;
                            position += matchedToken.Length;
                            break;
                        }
                        else
                        {
                            matchedToken = null;
                        }
                    }
                }
                if(matchedToken == null)
                {
                    throw new LexerException(
                        String.Format("Invalid character '{0}' at position {1}, line {2}", input[total], position, line)
                    );
                }
            }
            return result;
        }
    }
}
