﻿namespace GFloydLib
{
    public enum Instruction
    {
        Pushglobal,
        Pushint,
        Push,
        Mkap,
        Slide,
        Unwind,
        Update,
        Pop,
        Alloc,
        Eval,
        Add, Sub, Mult, Div,
        Eq, Neq, Lt, Le, Gt, Ge,
        Condition
    }
}
