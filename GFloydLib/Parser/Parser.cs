﻿using System;
using System.Collections.Generic;
using GFloydLib.Lexer;
using GFloydLib.Parser.Nodes;

namespace GFloydLib.Parser
{
    public class Parser
    {
        private int _currentTokenNumber;
        private readonly List<DataToken> _tokens;

        public Parser(List<DataToken> tokens)
        {
            this._tokens = tokens;
            this._tokens.Add(new DataToken() { Token = Token.End });
        }

        private void ParseError()
        {
            var dt = _tokens[_currentTokenNumber];
            throw new ParserException(
                String.Format("Invalid syntax '{0}' at position {1}, line {2}", dt.Value, dt.Position, dt.Line)
            );
        }

        private void NextToken()
        {
            if (_currentTokenNumber + 1 == _tokens.Count)
                return;
            ++_currentTokenNumber;
        }

        private bool HasNextToken()
        {
            return _tokens.Count - _currentTokenNumber > 1;
        }

        private bool Accept(Token token)
        {
            if (_tokens[_currentTokenNumber].Token == token)
            {
                NextToken();
                return true;
            }
            return false;
        }

        private void Expect(Token token) {
            if (!Accept(token))
                ParseError();
        }

        private bool LookupRelop(){
            for (var i = _currentTokenNumber; i < _tokens.Count; ++i){
                if (_tokens[i].Token == Token.LeftParent)
                    return false;
                if (_tokens[i].Token == Token.Less 
                    || _tokens[i].Token == Token.LessOrEqual
                    || _tokens[i].Token == Token.Equal
                    || _tokens[i].Token == Token.NotEqual
                    || _tokens[i].Token == Token.Greater
                    || _tokens[i].Token == Token.GreaterOrEqual)
                    return true;
            }
            return false;
        }

        public Node Parse()
        {
            if (_tokens.Count == 0)
                throw new ParserException("Sources are empty");
            var head = new HeadNode();
            Program(head);
            if(HasNextToken())
                throw new ParserException("Parse exception. Possible ';' not found");
            head.InvertApplication();
            return head;
        }

        private void Program(HeadNode head)
        {
            head.Nodes.Add(SuperCombinator());
            Program1(head);
        }

        private void Program1(HeadNode head)
        {
            if (Accept(Token.Semicolon))
            {
                if (_tokens[_currentTokenNumber].Token == Token.End)
                    return;
                Program(head);
            }
            // Если нет ; ?
        }

        private SuperCombinatorNode SuperCombinator()
        {
            var variable = Var();
            List<VarNode> vars = NVar(new List<VarNode>());
            Expect(Token.Assign);
            Node expression = Expression();
            return new SuperCombinatorNode(variable, vars, expression);
        }

        private VarNode Var()
        {
            if (Token.Var == _tokens[_currentTokenNumber].Token)
            {
                var variable = new VarNode(_tokens[_currentTokenNumber].Value);
                NextToken();
                return variable;
            }
            ParseError();
            return null;
        }

        private NumberNode Num()
        {
            if (Token.Num == _tokens[_currentTokenNumber].Token)
            {
                var number = new NumberNode(int.Parse(_tokens[_currentTokenNumber].Value));
                NextToken();
                return number;
            }
            ParseError();
            return null;
        }

        private List<VarNode> NVar(List<VarNode> vars)
        {
            if (Token.Assign != _tokens[_currentTokenNumber].Token)
            {
                vars.Add(Var());
                NVar(vars);
            }
            return vars;
        }

        private List<VarNode> LambdaVar(List<VarNode> variables)
        {
            if (Token.Point != _tokens[_currentTokenNumber].Token)
            {
                variables.Add(Var());
                NVar(variables);
            }
            return variables;
        }

        private Node Expression()
        {
            if(Accept(Token.Let)){
                List<Tuple<VarNode, Node>> defns = Defns(new List<Tuple<VarNode, Node>>());
                Expect(Token.In);
                Node expression = Expression();
                return new LetNode(false, defns, expression);
            }
            else if(Accept(Token.Letrec))
            {
                List<Tuple<VarNode, Node>> defns = Defns(new List<Tuple<VarNode, Node>>());
                Expect(Token.In);
                Node expression = Expression();
                return new LetNode(true, defns, expression);
            }
            else if (Accept(Token.Lambda))
            {
                List<VarNode> variables = new List<VarNode>();
                variables.Add(Var());
                LambdaVar(variables);
                Expect(Token.Point);
                Node expression = Expression();
                return new LambdaNode(variables, expression);
            } else if (Accept(Token.If))
            {
                return new CondNode(Aexpr(), Aexpr(), Aexpr());
            }
            else {
                return Expression1();
            }
        }

        private List<Tuple<VarNode, Node>> Defns(List<Tuple<VarNode, Node>> defns)
        {
            defns.Add(Defn());
            if (Accept(Token.Comma)) {
                Defns(defns);
            }
            return defns;
        }

        private Tuple<VarNode, Node> Defn(){
            VarNode variable = Var();
            Expect(Token.Assign);
            Node expression = Expression();
            return new Tuple<VarNode, Node>(variable, expression);
        }

        private Node Expression1()
        {
            Node expression2 = Expression2();
            if (Accept(Token.Or)){
                Node expression1 = Expression1();
                return new ApplicationNode(new ApplicationNode(new VarNode("|"), expression2), expression1);
            }
            return expression2;
        }

        private Node Expression2()
        {
            Node expression3 = Expression3();
            if (Accept(Token.And))
            {
                Node expression2 = Expression2();
                return new ApplicationNode(new ApplicationNode(new VarNode("&"), expression3), expression2);
            }
            return expression3;
        }

        private Node Expression3()
        {
            Node expression4_1 = Expression4();
            if (LookupRelop()){
                Token operation = RelOp();
				Relop relop;
                Node expression4_2 = Expression4();
				switch(operation){
				case Token.Equal:
					relop = Relop.Eq;
					break;
				case Token.Greater:
					relop = Relop.Gt;
					break;
				case Token.GreaterOrEqual:
					relop = Relop.Ge;
					break;
				case Token.Less:
					relop = Relop.Lt;
					break;
				case Token.LessOrEqual:
					relop = Relop.Le;
					break;
				case Token.NotEqual:
					relop = Relop.Ne;
					break;
				default:
					relop = Relop.Eq;
					break;
				}
                return new RelopNode(relop, expression4_1, expression4_2);
            }
            return expression4_1;
        }

        private Node Expression4()
        {
            Node expression5 = Expression5();
            if (Accept(Token.Plus))
            {
                Node expression4 = Expression4();
                return new ArithNode(Arith.Add, expression5, expression4);
            } else if (Accept(Token.Minus))
            {
                Node expression5_2 = Expression5();
                return new ArithNode(Arith.Sub, expression5, expression5_2);
            }
            return expression5;
        }

        private Node Expression5()
        {
            Node expression6 = Expression6();
            if (Accept(Token.Mult))
            {
                Node expression5 = Expression5();
                return new ArithNode(Arith.Mult, expression6, expression5);
            }
            else if (Accept(Token.Derive))
            {
                Node expression6_2 = Expression6();
                return new ArithNode(Arith.Div, expression6, expression6_2);
            }
            return expression6;
        }

        private Node Expression6()
        {
            Node aexpr = Aexpr();
            if (Token.Var == _tokens[_currentTokenNumber].Token 
                || Token.Num == _tokens[_currentTokenNumber].Token
                || Token.LeftParent == _tokens[_currentTokenNumber].Token)
            {
                Node expression6 = Expression6();
                return new ApplicationNode(aexpr, expression6);
            }
            return aexpr;
        }

        private Node Aexpr(){
            if (Token.Var == _tokens[_currentTokenNumber].Token)
            {
                return Var();
            }
            else if (Token.Num == _tokens[_currentTokenNumber].Token)
            {
                return Num();
            }
            else {
                Expect(Token.LeftParent);
                Node expression = Expression();
                expression.IsReadonly = true;
                Expect(Token.RightParent);
                return expression;
            }
        }

        private Token RelOp(){
            if (_tokens[_currentTokenNumber].Token == Token.Less
                    || _tokens[_currentTokenNumber].Token == Token.LessOrEqual
                    || _tokens[_currentTokenNumber].Token == Token.Equal
                    || _tokens[_currentTokenNumber].Token == Token.NotEqual
                    || _tokens[_currentTokenNumber].Token == Token.Greater
                    || _tokens[_currentTokenNumber].Token == Token.GreaterOrEqual)
            {
                Token operation = _tokens[_currentTokenNumber].Token;
                NextToken();
                return operation;
            }
            else {
                ParseError();
                return Token.Equal;
            }
        }
    }
}
