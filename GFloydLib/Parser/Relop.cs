using System;

namespace GFloydLib.Parser
{
	public enum Relop
	{
		Eq, Lt, Gt, Le, Ge, Ne
	}
}

