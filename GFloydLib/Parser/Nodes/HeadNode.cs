﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GFloydLib.Compiler;

namespace GFloydLib.Parser.Nodes
{
    public class HeadNode : Node
    {
        public List<Node> Nodes { get; private set; }

        public HeadNode()
        {
            Nodes = new List<Node>();
        }

        override public string Print()
        {
            string result = "";
            foreach (Node node in Nodes)
                result += node.Print() + "\n";
            return result;
        }

        public override void InvertApplication()
        {
            foreach (Node node in Nodes)
                node.InvertApplication();
        }

        public override void Accept(ICompiler compiler, bool eval)
        {
            compiler.Compile(this, eval);
        }
    }
}
