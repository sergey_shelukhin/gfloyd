﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GFloydLib.Compiler;

namespace GFloydLib.Parser.Nodes
{
    public class LambdaNode : Node
    {
        public List<VarNode> Variables { get; private set; }
        public Node Body { get; private set; }

        public LambdaNode(List<VarNode> variables, Node body)
        {
            Variables = variables;
            Body = body;
        }

        override public string Print()
        {   
            string variables_str = "";
            foreach(VarNode vn in Variables)
                variables_str += vn.Print() + " ";
            return string.Format("Lambda {0} -> {1}", variables_str, Body.Print());
        }

        public override void InvertApplication()
        {
            Body.InvertApplication();
        }

        public override void Accept(ICompiler compiler, bool eval)
        {
            compiler.Compile(this, eval);
        }
    }
}
