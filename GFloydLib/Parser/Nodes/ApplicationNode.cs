﻿using GFloydLib.Compiler;
using System.Collections.Generic;

namespace GFloydLib.Parser.Nodes
{
    public class ApplicationNode : Node
    {
        public Node Left { get; protected set; }
        public Node Right { get; protected set; }

        public ApplicationNode(Node left, Node right)
        {
            Left = left;
            Right = right;
        }

        override public string Print()
        {
            return string.Format("App({0}, {1})", Left.Print(), Right.Print());
        }

        public override void InvertApplication()
        {
            if (!Right.IsReadonly && Right is ApplicationNode && Left is ApplicationNode)
            {
                ApplicationNode right = (ApplicationNode)Right;
                Right = right.Right;
                Node right_left = right.Left;
                right.Left = Left;
                right.Right = right_left;
                Left = right;
                this.InvertApplication();
                return;
            }
            else if(!Right.IsReadonly && Right is ApplicationNode && !(Left is ApplicationNode))
            {
                ApplicationNode right = (ApplicationNode) Right;
                Left = new ApplicationNode(Left, right.Left);
                Right = right.Right;
                this.InvertApplication();
                return;
            }
            Left.InvertApplication();
            Right.InvertApplication();
        }

        public override void Accept(ICompiler compiler, Dictionary<string, int> env, int succ, bool eval)
        {
            compiler.Compile(this, env, succ, eval);
        }
    }
}
