﻿using GFloydLib.Compiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.Parser.Nodes
{
    public class ArithNode : ApplicationNode
    {
        public ArithNode(Arith operation, Node left, Node right) : base(left, right)
        {
            this.operation = operation;
            Left = left;
            Right = right;
        }

        override public string Print()
        {
            return string.Format("{0}({1}, {2})", operation, Left.Print(), Right.Print());
        }

        public Arith operation { get; private set; }
        public override void InvertApplication()
        {
            Left.InvertApplication();
            Right.InvertApplication();
        }

        public override void Accept(ICompiler compiler, Dictionary<string, int> env, int succ, bool eval)
        {
            compiler.Compile(this, env, succ, eval);
        }
    }
}
