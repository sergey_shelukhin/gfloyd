﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GFloydLib.Compiler;
using GFloydLib.Lexer;

namespace GFloydLib.Parser.Nodes
{
    public class VarNode : Node
    {
        public VarNode(string value)
        {
            Value = value;
        }

        public string Value { get; private set; }

        override public string Print()
        {
            return Value;
        }

        public override void InvertApplication(){ }
        public override void Accept(ICompiler compiler, Dictionary<string, int> env, int succ, bool eval)
        {
            compiler.Compile(this, env, succ, eval);
        }
    }
}
