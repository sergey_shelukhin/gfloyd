﻿using GFloydLib.Compiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.Parser.Nodes
{
    public class CondNode : ApplicationNode
    {
        public Node Condition { get; protected set; }

        public CondNode(Node condition, Node left, Node right) : base(left, right)
        {
            Condition = condition;
            Left = left;
            Right = right;
        }

        override public string Print()
        {
            return string.Format("if [{0}] [{1} :: {2}]", Condition.Print(), Left.Print(), Right.Print());
        }

        public Arith operation { get; private set; }

        public override void InvertApplication()
        {
            Condition.InvertApplication();
            Left.InvertApplication();
            Right.InvertApplication();
        }

        public override void Accept(ICompiler compiler, Dictionary<string, int> env, int succ, bool eval)
        {
            compiler.Compile(this, env, succ, eval);
        }
    }
}
