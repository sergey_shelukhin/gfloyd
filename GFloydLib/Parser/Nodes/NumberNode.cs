﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GFloydLib.Compiler;

namespace GFloydLib.Parser.Nodes
{
    public class NumberNode : Node
    {
        public int Value { get; private set; }

        public NumberNode(int value)
        {
            Value = value;
        }

        override public string Print()
        {
            return Value.ToString();
        }

        public override void InvertApplication(){ }
        public override void Accept(ICompiler compiler, Dictionary<string, int> env, int succ, bool eval)
        {
            compiler.Compile(this, env, succ, eval);
        }
    }
}
