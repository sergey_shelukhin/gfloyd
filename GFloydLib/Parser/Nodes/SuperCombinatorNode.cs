﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GFloydLib.Compiler;
using GFloydLib.Lexer;

namespace GFloydLib.Parser.Nodes
{
    public class SuperCombinatorNode : Node
    {
        public VarNode Name { get; private set; }
        public List<VarNode> Variables { get; private set; }
        public Node Body { get; private set; }

        public SuperCombinatorNode(VarNode name, List<VarNode> variables, Node body)
        {
            Name = name;
            Variables = variables;
            Body = body;
        }

        override public string Print()
        {
            string variables = "";
            foreach(VarNode vn in Variables)
                variables += vn.Print() + " ";
            return string.Format("SC {0} : {1} = {2}", Name.Print(), variables, Body.Print());
        }

        public override void InvertApplication()
        {
            Body.InvertApplication();
        }

        public override void Accept(ICompiler compiler, bool eval)
        {
            compiler.Compile(this, eval);
        }
    }
}
