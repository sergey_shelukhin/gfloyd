﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GFloydLib.Compiler;

namespace GFloydLib.Parser.Nodes
{
    public class LetNode : Node
    {
        public bool IsRec { get; private set; }
        public List<Tuple<VarNode, Node>> Definitions { get; private set; }
        public Node Body { get; private set; }

        public LetNode(bool isRec, List<Tuple<VarNode, Node>> definitions, Node body)
        {
            IsRec = isRec;
            Definitions = definitions;
            Body = body;
        }

        override public string Print()
        {
            var def_str = "";
            foreach (Tuple<VarNode, Node> tvn in Definitions)
                def_str += string.Format("{0} = {1}, ", tvn.Item1.Print(), tvn.Item2.Print());
            return string.Format("Let[rec={0}; {1}; {2}]", IsRec, def_str, Body.Print());
        }

        public override void InvertApplication()
        {
            foreach (Tuple<VarNode, Node> tvn in Definitions)
                tvn.Item2.InvertApplication();
            Body.InvertApplication();
        }

        public override void Accept(ICompiler compiler, Dictionary<string, int> env, int succ, bool eval)
        {
            compiler.Compile(this, env, succ, eval);
        }
    }
}
