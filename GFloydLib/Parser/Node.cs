﻿using GFloydLib.Compiler;
using System;
using System.Collections.Generic;

namespace GFloydLib.Parser
{
    public abstract class Node
    {
        public bool IsReadonly { get; set; }
        public abstract string Print();
        public abstract void InvertApplication();

        public virtual void Accept(ICompiler compiler, Dictionary<string, int> env, int succ, bool eval)
        {
            throw new NotImplementedException();
        }
        public virtual void Accept(ICompiler compiler, bool eval)
        {
            throw new NotImplementedException();
        }
    }
}
