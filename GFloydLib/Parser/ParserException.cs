﻿using System;

namespace GFloydLib.Parser
{
    public class ParserException : GFloydException
    {
        public ParserException()
        { }
        public ParserException(string message) : base(message) { }
        public ParserException(string message, Exception inner) : base(message, inner) { }
    }
}
