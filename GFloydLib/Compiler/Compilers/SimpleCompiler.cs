﻿using GFloydLib.Parser.Nodes;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GFloydLib.Compiler.Compilers
{
    public class SimpleCompiler : ICompiler
    {

        public Dictionary<string, ArrayList> map { get; private set; }
        protected string _branch;

        // RemoveMePlease
        private void HardCode()
        {
            map.Add("+", new ArrayList() {2, 
                Instruction.Push, 1, Instruction.Eval, Instruction.Push, 1, 
                Instruction.Eval, Instruction.Add, Instruction.Update, 2, 
                Instruction.Pop, 2, Instruction.Unwind
            });
			map.Add("-", new ArrayList() {2, 
				Instruction.Push, 1, Instruction.Eval, Instruction.Push, 1, 
				Instruction.Eval, Instruction.Sub, Instruction.Update, 2, 
				Instruction.Pop, 2, Instruction.Unwind
			});
			map.Add("/", new ArrayList() {2, 
				Instruction.Push, 1, Instruction.Eval, Instruction.Push, 1, 
				Instruction.Eval, Instruction.Div, Instruction.Update, 2, 
				Instruction.Pop, 2, Instruction.Unwind
			});
			map.Add("*", new ArrayList() {2, 
				Instruction.Push, 1, Instruction.Eval, Instruction.Push, 1, 
				Instruction.Eval, Instruction.Mult, Instruction.Update, 2, 
				Instruction.Pop, 2, Instruction.Unwind
			});
        }
        //

        public SimpleCompiler() 
        {
            map = new Dictionary<string, ArrayList>();
			HardCode();
        }

        public void Compile(ApplicationNode node, Dictionary<string, int> env, int succ, bool eval)
        {
            if(eval) 
            {
                node.Accept(this, env, succ, false);
                map[_branch].Add(Instruction.Eval);
            }
            else
            {
                node.Right.Accept(this, env, succ, eval);
                node.Left.Accept(this, env, succ + 1, eval);
                map[_branch].Add(Instruction.Mkap);
            }
        }

        public void Compile(HeadNode node, bool eval = true)
        {
            foreach (var n in node.Nodes)
                n.Accept(this, eval);
        }

        public void Compile(LambdaNode node, bool eval)
        {
            throw new System.NotImplementedException();
        }

        public void Compile(NumberNode node, Dictionary<string, int> env, int succ, bool eval)
        {
            map[_branch].Add(Instruction.Pushint);
            map[_branch].Add(node.Value);
        }

        virtual public void Compile(SuperCombinatorNode node, bool eval)
        {
            _branch = node.Name.Value;
            map.Add(_branch, new ArrayList());
            map[_branch].Add(node.Variables.Count);
            var env = new Dictionary<string, int>();
            for (var i = 0; i < node.Variables.Count; ++i)
                env.Add(node.Variables[i].Value, i);
            node.Body.Accept(this, env, 0, eval);
            map[_branch].Add(Instruction.Slide);
            map[_branch].Add(node.Variables.Count + 1);
            map[_branch].Add(Instruction.Unwind);
        }

        public void Compile(VarNode node, Dictionary<string, int> env, int succ, bool eval)
        {
            if (eval)
            {
                node.Accept(this, env, succ, false);
                map[_branch].Add(Instruction.Eval);
                return;
            }
            if(map.ContainsKey(node.Value))
            {
                map[_branch].Add(Instruction.Pushglobal);
                map[_branch].Add(node.Value);
            }
            else
            {
                if (!env.ContainsKey(node.Value))
                    throw new CompilerException(string.Format("Variable {0} is not declared", node.Value));
                map[_branch].Add(Instruction.Push);
                map[_branch].Add(env[node.Value] + succ);
            }
        }

        virtual public void Compile(LetNode node, Dictionary<string, int> env, int succ, bool eval)
        {
            throw new System.NotImplementedException();
        }

        virtual public void Compile(ArithNode node, Dictionary<string, int> env, int succ, bool eval)
        {
            throw new System.NotImplementedException();
        }

		virtual public void Compile(RelopNode node, Dictionary<string, int> env, int succ, bool eval)
		{
			throw new System.NotImplementedException();
		}

        virtual public void Compile(CondNode node, Dictionary<string, int> env, int succ, bool eval)
        {
            throw new System.NotImplementedException();
        }
    }
}
