﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GFloydLib.Parser.Nodes;

namespace GFloydLib.Compiler.Compilers
{
    public class LazyCompiler : SimpleCompiler
    {
        public override void Compile(SuperCombinatorNode node, bool eval)
        {
            _branch = node.Name.Value;
            map.Add(_branch, new ArrayList());
            map[_branch].Add(node.Variables.Count);
            var env = new Dictionary<string, int>();
            for (var i = 0; i < node.Variables.Count; ++i)
                env.Add(node.Variables[i].Value, i);
            node.Body.Accept(this, env, 0, eval);
            map[_branch].Add(Instruction.Update);
            map[_branch].Add(node.Variables.Count);
            map[_branch].Add(Instruction.Pop);
            map[_branch].Add(node.Variables.Count);
            map[_branch].Add(Instruction.Unwind);
        }

        private Dictionary<string, int> CreateEnvironment(List<Tuple<VarNode, Parser.Node>> defs) 
        {
            var env2 = new Dictionary<string, int>();
            for (var i = 0; i < defs.Count; ++i)
            {
                env2.Add(defs[i].Item1.Value, defs.Count - i - 1);
            }
            return env2;
        }

        public override void Compile(LetNode node, Dictionary<string, int> env, int succ, bool eval) 
        {
            if (node.IsRec) 
            {
                map[_branch].Add(Instruction.Alloc);
                map[_branch].Add(node.Definitions.Count);
                var env2 = CreateEnvironment(node.Definitions);
                foreach (var arg in env)
                    env2.Add(arg.Key, arg.Value + node.Definitions.Count + succ);
                for (var i = 0; i < node.Definitions.Count; ++i)
                {
                    node.Definitions[i].Item2.Accept(this, env2, 0, false);
                    map[_branch].Add(Instruction.Update);
                    map[_branch].Add(node.Definitions.Count - i - 1);
                }
                node.Body.Accept(this, env2, 0, eval);
                map[_branch].Add(Instruction.Slide);
                map[_branch].Add(node.Definitions.Count);
            } 
            else 
            {
                var env2 = new Dictionary<string, int>();
                for (var i = 0; i < node.Definitions.Count; ++i) 
                {
                    node.Definitions[i].Item2.Accept(this, env, succ + i, false);
                    env2.Add(node.Definitions[i].Item1.Value, node.Definitions.Count - i - 1);
                }
                foreach (var arg in env)
                    env2.Add(arg.Key, arg.Value + node.Definitions.Count + succ); // +succ?
                node.Body.Accept(this, env2, 0, eval);
                map[_branch].Add(Instruction.Slide);
                map[_branch].Add(node.Definitions.Count);
            }
        }

        public override void Compile(ArithNode node, Dictionary<string, int> env, int succ, bool eval) 
        {
			node.Right.Accept (this, env, succ, eval);
			node.Left.Accept (this, env, succ + 1, eval);
			if (eval) {
				switch (node.operation) {
				case Parser.Arith.Add:
					map [_branch].Add (Instruction.Add);
					break;
				case Parser.Arith.Div:
					map [_branch].Add (Instruction.Div);
					break;
				case Parser.Arith.Sub:
					map [_branch].Add (Instruction.Sub);
					break;
				case Parser.Arith.Mult:
					map [_branch].Add (Instruction.Mult);
					break;
				}
			} else 
			{
				map[_branch].Add(Instruction.Pushglobal);
				switch (node.operation) {
					case Parser.Arith.Add:
						map[_branch].Add("+");
						break;
					case Parser.Arith.Div:
						map[_branch].Add("/");
						break;
					case Parser.Arith.Sub:
						map[_branch].Add("-");
						break;
					case Parser.Arith.Mult:
						map [_branch].Add("*");
						break;
				}
				map[_branch].Add(Instruction.Mkap);
				map[_branch].Add(Instruction.Mkap);
			}
        }

		public override void Compile(RelopNode node, Dictionary<string, int> env, int succ, bool eval) 
		{
			node.Right.Accept(this, env, succ, eval);
			node.Left.Accept(this, env, succ + 1, eval);
			switch (node.operation) 
			{
				case Parser.Relop.Eq:
					map[_branch].Add(Instruction.Eq);
					break;
				case Parser.Relop.Ne:
					map[_branch].Add(Instruction.Neq);
					break;
				case Parser.Relop.Gt:
					map[_branch].Add(Instruction.Gt);
					break;
				case Parser.Relop.Ge:
					map[_branch].Add(Instruction.Ge);
					break;
				case Parser.Relop.Lt:
					map[_branch].Add(Instruction.Lt);
					break;
				case Parser.Relop.Le:
					map[_branch].Add(Instruction.Le);
					break;
			}
		}

        // FIXME: исправить всю быстроглупость
        public override void Compile(CondNode node, Dictionary<string, int> env, int succ, bool eval)
        {
            string bac_branch = _branch; 
            node.Condition.Accept(this, env, succ, eval);
            map[_branch].Add(Instruction.Condition);
            int count = map[_branch].Count;
            _branch = count++ + bac_branch;
            map[bac_branch].Add(_branch);
            map.Add(_branch, new ArrayList());
            node.Left.Accept(this, env, succ, eval);
            _branch = count + bac_branch;
            map[bac_branch].Add(_branch);
            map.Add(_branch, new ArrayList());
            node.Right.Accept(this, env, succ, eval);
            _branch = bac_branch;
        }
    }
}
