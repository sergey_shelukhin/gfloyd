﻿using GFloydLib.Parser.Nodes;
using System.Collections.Generic;

namespace GFloydLib.Compiler
{
    public interface ICompiler
    {
        void Compile(ApplicationNode node, Dictionary<string, int> env, int succ, bool eval = false);
        void Compile(HeadNode node, bool eval = false);
        void Compile(LambdaNode node, bool eval = false);
        void Compile(NumberNode node, Dictionary<string, int> env, int succ, bool eval = false);
        void Compile(SuperCombinatorNode node, bool eval = false);
        void Compile(VarNode node, Dictionary<string, int> env, int succ, bool eval = false);
        void Compile(LetNode node, Dictionary<string, int> env, int succ, bool eval = false);
        void Compile(ArithNode node, Dictionary<string, int> env, int succ, bool eval = false);
        void Compile(CondNode node, Dictionary<string, int> env, int succ, bool eval = false);
		void Compile(RelopNode node, Dictionary<string, int> env, int succ, bool eval = false);
    }
}
