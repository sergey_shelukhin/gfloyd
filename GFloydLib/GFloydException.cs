﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib
{
    public class GFloydException : Exception
    {
        public GFloydException()
        { }
        public GFloydException(string message) : base(message) { }
        public GFloydException(string message, Exception inner) : base(message, inner) { }
    }
}
