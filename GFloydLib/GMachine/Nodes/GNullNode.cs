﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.GMachine
{
    class GNullNode : GNode
    {
        public GNullNode(int id)
        {
            Id = id;
        }

        override public string Print()
        {
            return "Null";
        }
    }
}
