﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.GMachine
{
    class GFuncNode : GNode
    {
        public string Value;

        public GFuncNode(string value, int id) 
        {
            Value = value;
            Id = id;
        }

        override public string Print()
        {
            return Value;
        }
    }
}
