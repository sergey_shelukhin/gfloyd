﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.GMachine
{
    abstract class GNode
    {
        public int Id { get; protected set; }
        public abstract string Print();
    }
}
