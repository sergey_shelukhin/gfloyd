﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.GMachine
{
    class GIntNode : GNode
    {
        public int Value;

        public GIntNode(int value, int id)
        {
            Id = id;
            Value = value;
        }

        override public string Print()
        {
            return Value.ToString();
        }
    }
}
