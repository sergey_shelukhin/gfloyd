﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.GMachine.Nodes
{
    class GIndNode : GNode
    {
        public GNode Node { get; set; }
        public GIndNode(GNode node, int id)
        {
            Id = id;
            Node = node;
        }

        override public string Print() 
        {
            return string.Format("Indirection({0})", Node.Print());
        }
    }
}
