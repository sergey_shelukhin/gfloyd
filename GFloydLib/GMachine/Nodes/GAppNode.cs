﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.GMachine
{
    class GAppNode : GNode
    {
        public GNode Left { get; set; }
        public GNode Right { get; set; }
        public GAppNode(GNode left, GNode right, int id)
        {
            Id = id;
            Left = left;
            Right = right;
        }

        override public string Print() 
        {
            return string.Format("App({0}, {1})", Left.Print(), Right.Print());
        }
    }
}
