﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.GMachine
{
    class GStack
    {
        private LinkedList<GNode> stack;
        public int Count 
        {
            get { return stack.Count; }
        }

        public GStack()
        {
            stack = new LinkedList<GNode>();
        }

        public void Push(GNode elem)
        {
            stack.AddFirst(elem);
        }

        public GNode Peek()
        {
            return stack.First();
        }

        public GNode Last() 
        {
            return stack.Last();
        }

        public GNode Pop()
        {
            var first = stack.First();
            stack.RemoveFirst();
            return first;
        }

        public void Clear() 
        {
            stack.Clear();
        }

        public void Rearrange(int n)
        {
            var listNode = stack.First;
            for (int i = 0; i < n - 1; ++i) 
            {
                listNode.Value = ((GAppNode) listNode.Value).Right;
                listNode = listNode.Next;
            }
            stack.AddBefore(listNode, 
                new LinkedListNode<GNode>(((GAppNode)listNode.Value).Right)
            );
        }

        public GNode this[int i]
        {
            get { return stack.ElementAt(i); }
        }

        public void Slide(int n) 
        {
            var top = Pop();
            for (int i = 0; i < n; ++i)
            {
                stack.RemoveFirst();
            }
            Push(top);
        }
    }
}
