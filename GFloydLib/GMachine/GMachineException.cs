﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFloydLib.GMachine
{
    public class GMachineException : GFloydException
    {
        public GMachineException()
        { }
        public GMachineException(string message) : base(message) { }
        public GMachineException(string message, Exception inner) : base(message, inner) { }
    }
}
