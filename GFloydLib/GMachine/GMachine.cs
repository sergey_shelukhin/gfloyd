﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GFloydLib.GMachine.Nodes;
using System;
using System.Diagnostics;

namespace GFloydLib.GMachine
{
    public class GMachine
    {
        private struct Stats
        {
            public Stopwatch stopwatch;
            public int Steps;
            public int CreatedObjects;
            public int Reused;

            public string GetStats()
            {
                var result = string
                    .Format("Stats[Steps={0}, CreatedObjects={1}, Reused={2}, Time={3}]", 
                        Steps, CreatedObjects, Reused, stopwatch.Elapsed.TotalSeconds);
                return result;
            }
        }

        private ArrayList _code;
        private GStack stack;
        private Stack<Tuple<int, ArrayList, GStack>> dump;
        private Dictionary<string, ArrayList> globals;
        private Dictionary<int, GNode> heap;
        private const string ExecFunction = "main";
        private int _id;
        private Stats _stats;


        public GMachine(Dictionary<string, ArrayList> globals) 
        {
            stack = new GStack();
            heap = new Dictionary<int, GNode>();
            dump = new Stack<Tuple<int, ArrayList, GStack>>();
            _stats = new Stats();
            _stats.stopwatch = new Stopwatch();
            this.globals = globals;
            if (!globals.ContainsKey(ExecFunction))
                throw new GMachineException("Function 'main' is not found");
            stack.Push(new GFuncNode(ExecFunction, _id++));
        }

        public string StackTop() 
        {
            var node = stack.Peek();
            return node.Print();
        }

        public string StackTrace()
        {
            var result = "Stack{\n";
            for (var i = stack.Count - 1; i >= 0; --i)
                result += string.Format("[{0}]: {1}\n", i, stack[i].Print());
            return result += "}\n";
        }

        public void Execute()
        {
            _stats.stopwatch.Start();
            _code = globals[((GFuncNode)stack.Peek()).Value];
            while (_Execute());
            _stats.stopwatch.Stop();
            _stats.CreatedObjects = _id;
        }

        private bool _Execute()
        {
            for (var i = 1; i < _code.Count; ++i)
            {
                ++_stats.Steps;
                switch ((Instruction)_code[i])
                {
                    case Instruction.Pushglobal:
                        var funcName = (string)_code[++i];
                        if (!globals.ContainsKey(funcName))
                            throw new GMachineException(string.Format("Function {0} is not found", funcName));
                        stack.Push(new GFuncNode(funcName, _id++));
                        break;
                    case Instruction.Pushint:
                        var intValue = (int)_code[++i];
                        stack.Push(new GIntNode(intValue, _id++));
                        break;
                    case Instruction.Mkap:
                        var left = stack.Pop();
                        var right = stack.Pop();
                        stack.Push(new GAppNode(left, right, _id++));
                        break;
                    case Instruction.Push:
                        var node = stack[(int)_code[++i]];
                        stack.Push(node);
                        break;
                    case Instruction.Update:
                        var upval = (int)_code[++i];
                        var top = stack.Pop();
                        heap.Add(stack[upval].Id, top);
                        break;
                    case Instruction.Pop:
                        var popval = (int)_code[++i];
                        while (popval > 0)
                        {
                            --popval;
                            stack.Pop();
                        }
                        break;
                    case Instruction.Slide:
                        stack.Slide((int)_code[++i]);
                        break;
                    case Instruction.Alloc:
                        var n = (int)_code[++i];
                        while(n > 0)
                        {
                            stack.Push(new GNullNode(_id++));
                            --n;
                        }
                        break;
                    case Instruction.Eval:
                        var evalTop = stack.Pop();
                        // TODO: оптимизировать
                        dump.Push(new Tuple<int, ArrayList, GStack>(i, _code, stack));
                        _code = new ArrayList() { _code[0], Instruction.Unwind };
                        stack = new GStack();
                        stack.Push(evalTop);
                        i = 0;
                        break;
                    case Instruction.Add:
                        stack.Push(new GIntNode(
                            ((GIntNode)stack.Pop()).Value + 
                            ((GIntNode)stack.Pop()).Value, _id++)
                        );
                        break;
                    case Instruction.Sub:
                        stack.Push(new GIntNode(
                            ((GIntNode)stack.Pop()).Value -
                            ((GIntNode)stack.Pop()).Value, _id++)
                        );
                        break;
					case Instruction.Mult:
                        stack.Push(new GIntNode(
                            ((GIntNode)stack.Pop()).Value *
                            ((GIntNode)stack.Pop()).Value, _id++)
                        );
                        break;
                    case Instruction.Div:
                        stack.Push(new GIntNode(
                            ((GIntNode)stack.Pop()).Value /
                            ((GIntNode)stack.Pop()).Value, _id++)
                        );
                        break;
					case Instruction.Eq:
						stack.Push(new GIntNode(
							((GIntNode)stack.Pop()).Value == 
							((GIntNode)stack.Pop()).Value ? 1 : 0, _id++)
			            );
						break;
					case Instruction.Neq:
						stack.Push(new GIntNode(
							((GIntNode)stack.Pop()).Value !=
							((GIntNode)stack.Pop()).Value ? 1 : 0, _id++)
		    	        );
						break;
					case Instruction.Gt:
						stack.Push(new GIntNode(
							((GIntNode)stack.Pop()).Value >
							((GIntNode)stack.Pop()).Value ? 1 : 0, _id++)
					    );
						break;
					case Instruction.Ge:
						stack.Push(new GIntNode(
							((GIntNode)stack.Pop()).Value >=
							((GIntNode)stack.Pop()).Value ? 1 : 0, _id++)
			           );
					   break;
					case Instruction.Lt:
						stack.Push(new GIntNode(
							((GIntNode)stack.Pop()).Value <
							((GIntNode)stack.Pop()).Value ? 1 : 0, _id++)
						);
						break;
					case Instruction.Le:
						stack.Push(new GIntNode(
							((GIntNode)stack.Pop()).Value <=
							((GIntNode)stack.Pop()).Value ? 1 : 0, _id++)
			            );
						break;
                    // FIXME: после экзамена
                    case Instruction.Condition:
                        int condval = ((GIntNode) stack.Pop()).Value;
                        string fst_br = (string) _code[++i];
                        string snd_br = (string) _code[++i];
                        if (condval != 0)
                        {
                            _code.InsertRange(i + 1, globals[fst_br]);
                        }
                        else 
                        {
                            _code.InsertRange(i + 1, globals[snd_br]);
                        }
                        break;
                    case Instruction.Unwind:
                        if (heap.ContainsKey(stack.Peek().Id))
                        {
                            ++_stats.Reused;
                            stack.Push(heap[stack.Pop().Id]);
                        }
                        if (stack.Peek() is GIntNode && !(dump.Count == 0))
                        {
                            var tuple = dump.Pop();
                            var topUnwind = stack.Pop();
                            i = tuple.Item1;
                            _code = tuple.Item2;
                            stack = tuple.Item3;
                            stack.Push(topUnwind);
                        }
                        if (stack.Peek() is GFuncNode && !(dump.Count == 0))
                        {
                            int argcount = (int) globals[((GFuncNode)stack.Peek()).Value][0];
                            GNode last = stack.Last();
                            if (stack.Count < argcount) 
                            {
                                var tuple = dump.Pop();
                                var topUnwind = stack.Pop();
                                i = tuple.Item1;
                                _code = tuple.Item2;
                                stack = tuple.Item3;
                                stack.Push(topUnwind);
                                stack.Push(last);
                                break;
                            }
                        }
                        while (stack.Peek() is GAppNode)
                        {
                            if (heap.ContainsKey(stack.Peek().Id))
                            {
                                ++_stats.Reused;
                                stack.Push(heap[stack.Pop().Id]);
                                continue;
                            }
                            stack.Push(((GAppNode)stack.Peek()).Left);
                        }
                        if (stack.Peek() is GFuncNode)
                        {
                            _code = globals[((GFuncNode)stack.Pop()).Value];
                            stack.Rearrange((int)_code[0]);
                            return true;
                        }
                        break;
                }
            }
            return false;
        }

        public string GetStats()
        {
            return _stats.GetStats();
        }
    }
}
